package com.example.interview;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    String res;
    String url = "https://randomuser.me/api/?page=1&results=1";
    ArrayList<accountdetais> userArrayList;
    Useradapter useradapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView=findViewById(R.id.tvview);
        userArrayList = new ArrayList<>();
        //userdetails();
        useradapter= new Useradapter(this, userArrayList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(useradapter);
        userdetails();
    }

    private void userdetails() {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Toast.makeText(MainActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {


                res = response.body().string();
                if (response.isSuccessful()) {
                    try {
                        JSONObject object = new JSONObject(res);
                        Log.d("Responce", "Code: " + response.code() + "message: " + object.toString());
                        JSONArray jsonArray = object.getJSONArray("results");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject obj = jsonArray.getJSONObject(i);
                            accountdetais accountdetais = new accountdetais();
                            accountdetais.setPicture(obj.getString("picture"));
                            accountdetais.setName(obj.getString("name"));
                            accountdetais.setEmail(obj.getString("email"));
                            accountdetais.setDob(obj.getString("dob"));
                            accountdetais.setPhone(obj.getString("phone"));
                            accountdetais.setUsername(obj.getString("organicsnake461"));
                            //Log.d("hello",accountdetais.toString());
                            userArrayList.add(accountdetais);
                            //Log.d("userlist", userArrayList.toString());
                            //recyclerView.setVisibility(View.VISIBLE);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            useradapter.loadata(userArrayList);
                            //Toast.makeText(Services.this, "" + userArrayList.size(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }
}