<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <TextView
        android:id="@+id/textViewTitle"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_gravity="center"
        android:gravity="center"
        android:text="User List"
        android:textColor="@android:color/black"
        android:textStyle="bold"
        android:padding="5dp"
        android:textSize="20dp" />

    <RelativeLayout
        android:id="@+id/heder"
        android:layout_width="match_parent"
        android:layout_height="200dp"
        android:layout_below="@id/textViewTitle"
        android:orientation="horizontal">

        <ImageView
            android:id="@+id/tvimg"
            android:layout_width="100dp"
            android:layout_height="100dp">
        </ImageView>
        <RelativeLayout
            android:id="@+id/view2"
            android:layout_width="match_parent"
            android:layout_height="200dp"
            android:layout_marginLeft="100dp"
            android:orientation="horizontal">

            <TextView
                android:id="@+id/tv_name"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:text="Name"
                android:layout_marginLeft="5dp"
                android:layout_marginHorizontal="5dp"
                android:gravity="center"
                android:textColor="#000000"
                android:textSize="24sp"
                android:textStyle="bold" />

            <TextView
                android:id="@+id/tv_email"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:text="E-mail"
                android:layout_marginLeft="5dp"
                android:layout_marginVertical="5dp"
                android:layout_below="@id/tv_name"
                android:textColor="#000000"
                android:textSize="24sp"
                android:textStyle="bold" />

            <TextView
                android:id="@+id/tv_date"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:text="Birth-date"
                android:layout_marginLeft="5dp"
                android:layout_marginVertical="5dp"
                android:layout_below="@id/tv_email"
                android:textColor="#000000"
                android:textSize="24sp"
                android:textStyle="bold" />

            <TextView
                android:id="@+id/tv_phonenumber"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:text="phone-no"
                android:layout_marginLeft="5dp"
                android:layout_marginVertical="5dp"
                android:layout_below="@id/tv_date"
                android:textColor="#000000"
                android:textSize="24sp"
                android:textStyle="bold" />
            <TextView
                android:id="@+id/tv_username"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:text="username"
                android:layout_marginLeft="5dp"
                android:layout_marginVertical="5dp"
                android:layout_below="@id/tv_phonenumber"
                android:textColor="#000000"
                android:textSize="24sp"
                android:textStyle="bold" />


        </RelativeLayout>

    </RelativeLayout>

</RelativeLayout>